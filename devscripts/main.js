'use strict';

document.addEventListener('DOMContentLoaded', () => {

  // Toggles

	var $toggler = $('.js-toggle-trigger');

	$toggler.each(function ($el) {
		$(this).on('click', (e) => {
			e.preventDefault();
			var target = $(this).data('target');
			var $target = $('#' + target);
			$(this).toggleClass('active');
			$target.toggleClass('active');
		});
	});

	// header menu
	var $mobileMenu = $('.header_menu');
	if ($mobileMenu.length > 0) {
		$mobileMenu.on('click', (e) => {
			e.preventDefault();
			$('body').toggleClass('mmenu-opened');
		});
	}

	// close mobile menu
	$('.pageCover').on('click', () => {
		$('.header_menu').click();
	});

	// close mobile search
	$('.searchWrap').on('click', (e) => {
		if ($(e.target).hasClass('active')) {
			$('.searchWrap').removeClass('active');
		}
	});

	// footer accordion when mobile
	if ($(window).width() < 769) {
		$('.js-footer-toggle').bind('click', footerToggle);
		$('body').bind('click', closeMemberNav);
	}
	$(window).resize(function() {
		if ($(window).width() < 769) {
			$('.js-footer-toggle').bind('click', footerToggle);
			$('body').bind('click', closeMemberNav);
		} else {
			$('.js-footer-toggle').unbind('click', footerToggle);
			$('body').unbind('click', closeMemberNav);
		}
	});

	// 3 columns carousel
	$('.js-carousel').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		// centerMode: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 2000,
				settings: {
					slidesToShow: 4,
					arrows: true
					// centerMode: false
				}
			},
			{
				breakpoint: 1009,
				settings: {
					slidesToShow: 3,
					arrows: false
					// centerMode: false
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					centerMode: true,
					centerPadding: '17%',
					arrows: false
				}
			}
		]
	});


	// magnific popup
	$('.js-mfp-inline').each((index, ele) => {
		$(ele).magnificPopup({
			type: 'inline'
		});
	});

	function footerToggle(event) {
		event.preventDefault();
		$(this).toggleClass('active');
		$(this).next('.submenu').toggleClass('active');
	}

	function closeMemberNav() {
		if ($(event.target).parents('.memberNav').length === 0) {
			if ($('.memberNav_toggle').hasClass('active')) {
				$('.memberNav_toggle').click();
			}
		}
	}
});